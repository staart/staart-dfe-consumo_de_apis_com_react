# Consumo de APIs com React

Projeto criado para aprender sobre conexão entre servidores e clientes utilizando o React, entendendo como trabalhar com requisições HTTP para o consumo de serviços externos que são responsáveis por manter toda a lógica de dados por trás de uma aplicação, independentemente da biblioteca ou framework que estiver utilizando.
